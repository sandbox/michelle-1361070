<?php

/**
 * Description of ArtesianPostMetadataController
 */
class ArtesianPostMetadataController extends EntityDefaultMetadataController {

  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();
    $properties = &$info[$this->type]['properties'];

    $properties['author'] = array(
      'label' => t("Artesian Post Author"),
      'type' => 'user',
      'description' => t("The author of the post."),
      'getter callback' => 'entity_property_getter_method',
      'setter callback' => 'entity_property_setter_method',
      'setter permission' => 'administer profiles',
      'required' => TRUE,
      'schema field' => 'author_id',
    );

    $properties['thread'] = array(
      'label' => t("Artesian Thread"),
      'type' => 'artesian_thread',
      'description' => t("The thread of the post."),
      'getter callback' => 'entity_property_getter_method',
      'setter callback' => 'entity_property_setter_method',
      'setter permission' => 'administer artesian',
      'required' => TRUE,
      'schema field' => 'thread_id',
    );

    return $info;
  }

}

?>
