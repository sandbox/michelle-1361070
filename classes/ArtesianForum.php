<?php

/**
 * The Forum entity class
 */
class ArtesianForum extends ArtesianEntity {

  // Properties coresponding to fields in {artesian_forum}.
  public $forum_id = 0;
  public $name = '';
  public $parent_id = 0;
  public $weight = 0;
  public $depth = 0;
  public $listed = 1;
  public $owner_id = 0;
  public $thread_count = 0;
  public $post_count = 0;
  public $post_count_all = 0;
  public $last_thread_id = 0;
  public $last_post_id = 0;
  public $last_post_time = 0;

  // Aggregated totals that include totals from all child forums that are
  // accessable to current user.
  public $aggregateThreadCount = 0;
  public $aggregatePostCount = 0;
  public $aggregatePostCountAll = 0;
  public $aggregateLastPost = 0;

  // Child forums accessable to current user.
  public $children = array();

  public $ancestry = array();

  // Holds the render object for this forum type.
  protected $renderer = NULL;

  /*
   * Perform initial setup when the forum is instanciated.
   */
  public function __construct(array $values = array(), $entityType = NULL) {
    // Run this for the parent class.
    parent::__construct($values, $entityType);

    // Set the class for the renderer to use a bundle-specific class if it
    // exists or the default class if it does not.
    $class = str_replace('_', '', $this->entityType()) . 'Renderer';
    $class = (class_exists($class)) ? $class : 'ArtesianForumRenderer';
    $this->renderer = new $class;
  }

  /**
   * Returns the identifier of this forum.
   */
  public function id() {
    return $this->forum_id;
  }

  /**
   * Returns the name of the forum.
   */
  public function label() {
    return $this->name;
  }


  // @todo This get and set is a collosal hack for the moment. It's here to
  // provide the interface so I can start using it elsewhere but the internals
  // will need to be massivly reworked pending the results of the D8 issue.
  public function get($property_name) {
    return field_get_items('artesian_forum', $this, $property_name);
  }

  public function set($property_name, $value) {
    switch ($property_name) {
      case ('description'):
        $this->setDescription($value);
        break;
    }
  }

  private function setDescription($description) {
    // @todo This needs to be replaced, obviously.
    $this->artesian_forum_description['und'][0]['value'] = $description;
  }


// FORUM HIERARCHY ------------------------------------------------------------\


  /**
   * Retrieves a list of ancestors of this forum.
   *
   * This function uses the {artesian_forum} table rather than the
   * {artesian_forum_ancestor} table because it is used as the source of the
   * data that is to be written to {artesian_forum_ancestor} when a forum
   * is added or updated.
   *
   * @todo Find a better name than "create".
   *   Not using "load" because it's not loading from the ancestor table as per
   *   the note above. "create" is closer but not quite right, I don't think.
   *
   */
  public function createAncestry() {
    // Because our forum table will normally be small, we load the ID of every
    // forum and its parent and will sort through it in code.
    $query = db_select('artesian_forum', 'af');
    $query->fields('af', array('forum_id', 'parent_id'));
    $results = $query->execute();

    $forum_list = $results->fetchAllKeyed();

    // Start us off with this object's ID.
    $current_id = $this->id();

    // Initialize the array with this object's ID. We consider self an
    // ancestor to simplify some queries.
    $ancestry = array($this->id());

    // Traverse the list from parent to parent and add them to the ancestry.
    while ($forum_list[$current_id] > 0) {
      $ancestry[] = $forum_list[$current_id];
      $current_id = $forum_list[$current_id];
    }

    // Put the depth (the key) in the correct order.
    $ancestry = array_reverse($ancestry);

    return $ancestry;
  }

  /**
   * Loads the descendents of a forum in tree form.
   *
   * @param int $forum_id
   */
  public function loadDescendents($forum_id = NULL) {
    $forum_id = is_null($forum_id) ? $this->id() : $forum_id;

    $results = db_select('artesian_forum_ancestor', 'afs')
      ->fields('afs', array('ancestor_id', 'forum_id', 'depth'))
//      ->condition('ancestor_id', $forum_id)
      ->orderBy('depth')
      ->execute();

    $forum_list = array();
    foreach ($results as $record) {
      $forum_list[] = $record;
    }

//    dsm($forum_list);
  }

  /**
   * Loads an array of forum objects that are children of this object.
   *
   * @param int $depth
   *   (optional) Pass in a $depth greater than 2 to build a tree to the desired
   *   depth under this object. Defaults to 2 levels, which is what is used
   *   internally. Building a tree this way is not efficient and care should
   *   be taken to not use a large depth if you have many forums.
   */
  public function loadChildren($depth = 2) {
    $query = new EntityFieldQuery();

    $query->entityCondition('entity_type', 'artesian_forum')
      ->propertyCondition('parent_id', $this->id());

    $result = $query->execute();

    if (count($result) > 0) {
      // If children have been found, get the keys, which are their forum_ids and
      // then load all the child forum objects.
      $child_forum_ids = array_keys($result['artesian_forum']);
      $this->children = entity_load('artesian_forum', $child_forum_ids);

      // If we have more levels to do, tell each of the children to get their
      // children. We subtract one from the depth we are passing in so the
      // tree will stop building when it runs out of requested levels.
      // @todo Because we stop at 0, we can't use the normal 0 for unlimited.
      //   Determine whether this is really an issue and work-around if so.
      if ($depth > 1) {
        foreach ($this->children as $child) {
          $child->loadChildren($depth - 1);
        }
      }
    }
  }


  /**
   * Renders all parts of this forum.
   *
   * This function passes the responsibility to a specialized class in order to
   * allow different bundle types to handle their rendering in different ways.
   *
   * @return string
   *   HTML output of this forum.
   */
  public function render() {
    return $this->renderer->render($this);
  }
  public function renderChildList() {
    return $this->renderer->renderChildList($this);
  }
  public function renderChildren() {
    return $this->renderer->renderChildren($this);
  }
  public function renderThreads() {
    return $this->renderer->renderThreads($this);
  }


// FORMS ----------------------------------------------------------------------\

  public function getAddForm() {
    return entity_get_controller($this->entityType)->getAddForm($this);
  }

  public function getEditForm() {
    return entity_get_controller($this->entityType)->getEditForm($this);
  }

  public function getDeleteForm() {
    return entity_get_controller($this->entityType)->getDeleteForm($this);
  }


// STATISTICS -----------------------------------------------------------------\


  /**
   * Loads the thread/post counts for only this forum (not child forums).
   */
  public function loadCounts() {
    $query = db_select('artesian_thread', 'at');
    $query->join('artesian_forum_thread_relation', 'aftr', 'at.thread_id = aftr.thread_id');
    $query->addExpression('count(at.thread_id)', 'thread_count');
    $query->addExpression('SUM(at.post_count)', 'post_sum');
    $query->addExpression('SUM(at.post_count_all)', 'post_all_sum');
    $query->condition('at.published', 1)
      ->condition('aftr.forum_id', $this->forum_id);

    $result = $query->execute();
    $counts = $result->fetchAssoc();

    $this->thread_count = $counts['thread_count'];
    $this->post_count = $counts['post_sum'];
    $this->post_count_all = $counts['post_all_sum'];

    return;
  }

  /**
   * Loads the last post for only this forum (not child forums).
   */
  public function loadLastPost() {
    // Find the most recently posted in thread in this forum.
    $query = db_select('artesian_thread', 'at');
    $query->join('artesian_forum_thread_relation', 'aftr', 'at.thread_id = aftr.thread_id');
    $result = $query
      ->fields('at', array('thread_id', 'title', 'created', 'post_count', 'view_count', 'last_post_id', 'last_post_time', 'last_post_author_display_id', 'last_post_author_display_name'))
      ->condition('at.published', 1)
      ->condition('aftr.forum_id', $this->forum_id)
      ->orderBy('at.last_post_time', 'DESC')
      ->range(0, 1)
      ->execute();

    $record = $result->fetchAssoc();
    if (!empty($record)) {
      // Fill in info about the last post from the thread's denormalized data.
      $this->last_post_id = $record['last_post_id'];
      $this->last_post_time = $record['last_post_time'];
      $this->last_post_author_display_id = $record['last_post_author_display_id'];
      $this->last_post_author_display_name = $record['last_post_author_display_name'];
    }
    else {
      $this->last_post_id = 0;
      $this->last_post_time = 0;
      $this->last_post_author_display_id = 0;
      $this->last_post_author_display_name = "";
    }
  }

  /**
   * Loads the aggregate thread/post counts from this and all child forums.
   *
   * @todo Incorporate access control so inaccessable forums are not summed.
   *
   * @return
   *   @todo Returns FALSE if it fails.
   */
  public function loadAggregateCounts() {
    $query = db_select('artesian_forum', 'af');
    $query->join('artesian_forum_ancestor', 'afa', 'af.forum_id = afa.forum_id');
    $query->addExpression('SUM(thread_count)', 'thread_sum');
    $query->addExpression('SUM(post_count)', 'post_sum');
    $query->addExpression('SUM(post_count_all)', 'post_all_sum');
    $query->condition('ancestor_id', $this->id());

    $counts = $query->execute()->fetchAssoc();

    $this->aggregateThreadCount = $counts['thread_sum'];
    $this->aggregatePostCount = $counts['post_sum'];
    $this->aggregatePostCountAll = $counts['post_all_sum'];

    return;
  }

   /**
    * Loads info from the last published post in this or any child forum.
    *
    */
  public function loadAggregateLastPost() {
    $query = db_select('artesian_forum', 'af');
    $query->join('artesian_forum_ancestor', 'afa', 'af.forum_id = afa.forum_id');
    $query->join('artesian_thread', 'at', 'af.last_thread_id = at.thread_id');
    $result = $query
      ->condition('ancestor_id', $this->id())
      ->orderBy('at.last_post_time', 'DESC')
      ->fields('at', array('last_post_id', 'last_post_time'))
      ->fields('at', array('last_post_author_display_id', 'last_post_author_display_name'))
      ->range(0, 1)
      ->execute();

    $record = $result->fetchAssoc();

    if (!empty($record)) {
      // Fill in info about the last post.
      $this->aggregateLastPost['last_post_id'] = $record['last_post_id'];
      $this->aggregateLastPost['last_post_time'] = $record['last_post_time'];
      $this->aggregateLastPost['last_post_author_id'] = $record['last_post_author_display_id'];
      $this->aggregateLastPost['last_post_author_name'] = $record['last_post_author_display_name'];
    }
  }

  /**
   * Loads the statistics for this forum from the database.
   */
  public function loadStatistics() {
    $this->loadCounts();
    $this->loadLastPost();
  }

// UTILITY METHODS ------------------------------------------------------------\

  /**
   * Returns the default URI.
   */
  protected function defaultUri() {
    // @todo Make the default path a setting.
    return array('path' => 'forum/' . $this->identifier());
  }
}
