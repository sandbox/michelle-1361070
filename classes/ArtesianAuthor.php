<?php

/**
 * @file
 * Definition of ArtesianAuthor.
 */

/**
 * Represents the author of a post.
 */
class ArtesianAuthor {
  // For debugging.
  public $instance_id;

  /**
   * The actual ID of the user that created the post.
   *
   * @see {artesian_post}.author_id
   */
  public $authorID;

  /**
   * The {user}.name of the user that created the post.
   *
   * This is derived from the $authorID and used on the post form but is not
   * stored in any Artesian table.
   */
  public $authorName;

  /**
   * The ID used for displaying information about the author of the post.
   *
   * Normally the same as $authorID, it may be 0 if an authenticated user posted
   * anonymously. In rare cases may be a different user from the $authorID. This
   * is an edge case allowed by the software but not encouraged.
   *
   * @see {artesian_post}.author_display_id
   */
  public $authorDisplayID;

  /**
   * The displayed author name.
   *
   * This may be the username but also may be an alternate name such as that
   * supplied by the Real Name module or may be a pseudonym in the case of
   * anonymous posting.
   *
   * @see {artesian_post}.author_display_id
   */
  public $authorDisplayName;

  /**
   * The full user object of the display author of the post.
   *
   * For anonymous authors, this is the anonymous user object.
   *
   * @see user_load()
   * @see drupal_anonymous_user()
   */
  public $authorDisplayAccount;


  /**
   * Constructs the object.
   *
   * @param int $author_id
   * @param int $author_display_id
   * @param int $author_display_name
   */
  public function __construct($author_id, $author_display_id, $author_display_name) {
    $this->instance_id = time() . '__' . rand(1,100);
 //   dsm('Author ' . $this->instance_id . ' instantiated');
 //   dsm(debug_backtrace());

    $this->setAuthor($author_id, $author_display_id, $author_display_name);
  }

  /**
   * Sets the user (or visitor) this object represents.
   *
   * @param int|object $author
   *   (optional) ID or user object that corresponds with the actual author.
   *   Will be derived from current user if none passed in.
   * @param int|object $display_author
   *   (optional) ID or user object that corresponds with the displayed author.
   *   Will be derived from actual author if none passed in.
   * @param string $author_display_name
   *   (optional) Displayed name of the author. Will be derived from display
   *   author if none passed in.
   */
  public function setAuthor($author = NULL, $display_author = NULL, $author_display_name = NULL) {
    // Set the actual author's identity.
    if (is_null($author)) {
      // No author specified. Set to the current user.
      global $user;
      $author = clone $user;

      $this->authorID = $author->uid;
      $this->authorName = (!empty($author->name)) ? $author->name : '';

    }
    elseif (is_object($author)) {
      // A user object has been passed in for the author.
      $this->authorID = $author->uid;
      $this->authorName = (!empty($author->name)) ? $author->name : '';
    }
    else {
      // A UID has been passed in for the author (may be 0 and that's ok).
      $this->authorID = (int)$author;

      $author = user_load($this->authorID);
      $this->authorName = (!empty($author->name)) ? $author->name : '';
    }

    // Set the displayed identity.
    if (is_null($display_author)) {
      // We have not been given an alternate persona ID for the author of this
      // post so we set it to use the actual author.
      $this->authorDisplayID = $this->authorID;

      // Set the account object to the author account object. The code below
      // checks if the $author variable already is an object and uses it if it
      // is. Otherwise, it loads the user account associated with the author ID.
      $this->authorDisplayAccount = (is_object($author)) ? clone $author : user_load($this->authorDisplayID);
    }
    elseif (is_object($display_author)) {
      // An already loaded user object was passed in; copy it.
      $this->authorDisplayAccount = clone $display_author;
      $this->authorDisplayID = $this->authorDisplayAccount->uid;
    }
    else {
      // ID passed in for display author. Load the account that goes with it.
      $display_author = (int)$display_author;
      $this->authorDisplayAccount = user_load($display_author);
      $this->authorDisplayID = $this->authorDisplayAccount->uid;

    }

    // Set the displayed name.
    if (is_null($author_display_name)) {
      $this->authorDisplayName = $this->getDisplayNameFromUserObject($this->authorDisplayAccount);
    }
    else {
      $this->authorDisplayName = $author_display_name;
    }
 }

 /**
  * Takes a user object and returns the formatted user name.
  *
  * @param object $account
  */
  private function getDisplayNameFromUserObject($account) {
    return (empty($account->name)) ? variable_get('anonymous', t('Anonymous')) : format_username($account);
  }

}