<?php

/**
 * The ArtesianPostDisplay class
 */
class ArtesianPostRenderer {
  public $title;
  public $postedOn;
  public $postID;
  public $postNumber;
  public $permalink;
  public $rating;
  public $inReplyTo;

  public $content;

  public $editLink;
  public $deleteLink;
  public $reportLink;
  public $replyLink;

  public $authorName;
  public $authorPicture;
  public $authorJoined;
  public $authorPostCount;
  public $authorSignature;
  public $authorTitle;

  public function __construct($post) {
    $this->title = check_plain($post->label());

    $this->postID = $post->id();

    $post_date = new ArtesianDate($post->created);
    $this->postedOn = $post_date->date();

    $this->postNumber = 42;

    $this->permalink = l($this->postNumber, $post->viewURL());

    $this->rating = "5 stars";

    $this->inReplyTo = t("In reply to @post", array('@post' => $post->parent_id));

    $this->authorName = $post->author()->authorDisplayName;
    $this->authorPostCount = 42;

    $this->content = $post->buildContent();

    $this->editLink =  l(t('edit'), $post->editURL());
    $this->deleteLink =  l(t('delete'), $post->deleteURL());
    $this->replyLink =  l(t('reply'), $post->replyURL());
  }

  public function fillVariables(&$variables) {
    $variables['title'] = $this->title;
    $variables['post_id'] = $this->postID;
    $variables['posted_on']  = $this->postedOn;
    $variables['post_number'] = $this->postNumber;
    $variables['permalink'] = $this->permalink;
    $variables['in_reply_to'] = $this->inReplyTo;
    $variables['author_name']  = $this->authorName;
    $variables['author_post_count']  = $this->authorPostCount;
    $variables['content'] = $this->content;
    $variables['edit_link'] =  $this->editLink;
    $variables['delete_link'] =  $this->deleteLink;
    $variables['reply_link'] =  $this->replyLink;
  }

}