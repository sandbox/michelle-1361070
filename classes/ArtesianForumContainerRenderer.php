<?php

/**
 * Renders a forum.
 *
 * Default class for rendering a forum. Bundle types may each have their own
 * class to allow different types of forums to be rendered differently.
 *
 */
class ArtesianForumContainerRenderer {

  /**
   * Gathers information from $forum and renders it.
   *
   * @param object $forum
   *   The forum object to be rendered.
   *
   * @return string
   *   The HTML markup from rendering the forum.
   */
  public function render($forum) {
    $child_forums = $forum->renderChildren("Forums in " . $forum->name(), FALSE);

    $output = "<div>$child_forums</div>";
    return $output;
  }
}