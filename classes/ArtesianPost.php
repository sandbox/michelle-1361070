<?php

/**
 * The ArtesianPost entity class
 */
class ArtesianPost extends ArtesianEntity {
  // For debugging.
  public $instance_id;

  // Properties which corespond to fields in {artesian_post}.
  public $post_id;
  public $thread_id;
  public $anchor;
  public $parent_id;
  public $title;
  public $created;
  public $changed;
  public $published = 1;
  public $sticky;
  public $ip;
  public $hide_signature;
  public $position;

  // Author related properties that correspond to fields in {artesian_post} but
  // should only be accessed through the attached Author object.
  public $author_id;
  public $author_display_id;
  public $author_display_name;

  // ArtesianThread entity to which this post is assigned. Loaded on demand.
  protected $thread;

  // ArtesianAuthor object (not entity) that holds info about the post author.
  protected $author;

  public function __construct(array $values = array(), $entityType = NULL) {
    $this->instance_id = time() . '__' . rand(1,100);
  //  dsm('Post ' . $this->instance_id . ' instantiated');
  //  dsm(debug_backtrace());

    return parent::__construct($values, $entityType);
  }

  /**
   * Returns the identifier of this post.
   */
  public function id() {
    return $this->post_id;
  }

  /**
   * Returns the title of the post.
   */
  public function label() {
    return $this->title;
  }

  /**
   * Returns the URL to reply to the post.
   */
  public function replyURL() {
    $url = $this->url();
    return $url . '/reply';
  }

  /**
   * Returns the thread entity this post is assigned to.
   */
  public function thread() {
    if (empty($this->thread)) {
      // The thread hasn't yet been loaded. Do so.
      if (!empty($this->thread_id)) {
        $this->thread = artesian_thread_load($this->thread_id);
      }
    }

    return $this->thread;
  }

  /**
   * Returns the author object associated with this post, creating it if needed.
   */
  public function author($author_class = 'ArtesianAuthor') {
    if (empty($this->author)) {
      // Create the author object to hold author information.
      $this->author = new $author_class($this->author_id, $this->author_display_id, $this->author_display_name);
    }

    return $this->author;
  }

  // @todo This get and set is a collosal hack for the moment. It's here to
  // provide the interface so I can start using it elsewhere but the internals
  // will need to be massivly reworked pending the results of the D8 issue.
  public function get($property_name) {
    return field_get_items('artesian_post', $this, $property_name);
  }

  public function set($property_name, $value) {
    switch ($property_name) {
      case ('message'):
        $this->setMessage($value);
        break;
    }
  }

  private function setMessage($message) {
    $post_wrapper = entity_metadata_wrapper('artesian_post', $this);
    $post_wrapper->artesian_post_message->value->set($message);
  }

  public function getAddForm($thread) {
    return drupal_get_form('artesian_post_form', $this, $thread);
  }

  public function getEditForm($thread) {
    return drupal_get_form('artesian_post_form', $this, $thread);
  }

  public function getDeleteForm($thread) {
    return drupal_get_form('artesian_post_delete_confirm', $this, $thread);
  }

  /**
   * Determines this post's place in the thread hierarchy.
   *
   * @todo: This is a stub.
   *
   * @return string
   */
  public function position() {
    if (empty($this->position)) {
      $this->position = "01/01.00";
    }

    return $this->position;
  }

  public function render($template = 'artesian_post', $renderer = 'ArtesianPostRenderer') {
    $this->renderer = new $renderer($this);

    // Return the rendered HTML.
    return theme($template, array('post' => $this));
  }

  public function renderer() {
    return $this->renderer;
  }

  /**
   * Returns the default URI.
   *
   * This function is overridden to use a custom path.
   */
  protected function defaultUri() {
    // @todo Make the default path a setting.
    return array('path' => 'forum/thread/' . $this->thread_id . '/post/' . $this->id());
  }

  /**
   * Copies the values from the author object into the post's properties.
   *
   * This is normally done just prior to saving as the author object should be
   * used for accessing the data normally.
   *
   * @todo Consider using this to add a hook that fires when the author changes.
   */
  public function copyFromAuthor() {
    $this->author_id = $this->author->authorID;
    $this->author_display_id = $this->author->authorDisplayID;
    $this->author_display_name = $this->author->authorDisplayName;
  }

  public function permalink($link_text = NULL) {
    $link_text = (empty($link_text)) ? '#' . $this->post_id : $link_text;
    return l($link_text, $this->viewURL());
  }
}