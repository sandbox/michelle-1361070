<?php

/**
 * @file
 * Definition of ArtesianStyle.
 */

/**
 * Represents a forum style.
 */
class ArtesianStyle {
  public $path = '/styles/bones';
  public $name;
  public $machine_name;

  public function getStyle() {

  }

  public function path() {
    return $this->path;
  }

}
