<?php

/**
 * The Forum group entity class
 */
class ArtesianForumGroup extends Entity {

  // Properties coresponding to fields in {artesian_forum_group}.
  public $group_id = 0;
  public $name = '';
  public $base_path = '';
  public $thread_count = 0;
  public $post_count = 0;
  public $post_count_all = 0;
  public $last_thread_id = 0;
  public $last_post_id = 0;
  public $last_post_time = 0;

  // Aggregated totals that include totals from all contained forums that are
  // accessable to current user.
  public $aggregateThreadCount = 0;
  public $aggregatePostCount = 0;
  public $aggregatePostCountAll = 0;
  public $aggregateLastPost = 0;

  // Contained forums accessable to current user.
  public $children = array();

  // Holds the render object for this forum group type.
  protected $renderer = NULL;
}