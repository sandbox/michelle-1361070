<?php

/**
 * Renders a forum.
 *
 * Default class for rendering a forum. Bundle types may each have their own
 * class to allow different types of forums to be rendered differently.
 *
 */
class ArtesianForumRedirectRenderer {

  /**
   * Gathers information from $forum and renders it.
   *
   * @param object $forum
   *   The forum object to be rendered.
   *
   * @return string
   *   The HTML markup from rendering the forum.
   */
  public function render($forum) {

    $output = "Link forums don't contain forums or threads.";
    return $output;

  }
}