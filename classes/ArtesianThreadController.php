<?php

/**
 * @file
 */

class ArtesianThreadController extends EntityAPIController {

  /**
   * Performs some actions on thread creation.
   *
   * @param array $values
   *
   * @return object $thread
   */
  public function create(array $values = array()) {
    $thread = parent::create($values);

    // Create the anchor post.
    $thread->anchorPost();

    // Add this new thread to the forum in which it was created.
    // @todo: Need to do this.
    //$thread->addForumRelation($forum);

    return $thread;
  }

  /**
   * Saves the thread.
   *
   * @param $thread
   *   Thread object that needs to be saved.
   *
   * @return bool|int
   */
  public function save($thread) {
    // Set the changed and (if not already set) created times to the constant
    // that holds the time. REQUEST_TIME is used for performance reasons.
    $thread->changed = REQUEST_TIME;
    $thread->created = empty($thread->created) ? REQUEST_TIME : $thread->created;

    // Run the default entity save. Returns FALSE, SAVED_NEW or SAVED_UPDATED.
    $save_result = parent::save($thread);

    if ($save_result) {
      // Update thread relation table.
      $this->saveForumRelations($thread);

      // For each forum this thread is posted to, have the forum update its statistics.
      // @todo: Make forum relations work.
//      foreach ($thread->forumList as $forum_relation) {
//      if ($forum_relation->forum_id == $forum_relation->current_forum) {
//        // Only for forums where this is not a shadow post, load the forum and
//        // tell it to update its statistics.
//        $forum = artesian_forum_load($forum_relation->forum_id);
//        $forum->loadStatistics();
//        $forum->save();
//      }
      //   }
    }

    return $save_result;
  }

  public function saveForumRelations($thread) {
  }



//
//  /**
//   * Saves the list of forums this thread is in.
//   */
//  public function saveForums($thread) {
//    if ($thread->forumListChanged && !empty($thread->forumList)) {
//      $thread_id = $thread->id();
//
//      foreach ($thread->forumList as $forum_id => $current_forum) {
//        $values[] = array($thread_id, $forum_id, $current_forum);
//      }
//
//      $num_deleted = db_delete('artesian_forum_thread_relation')
//        ->condition('thread_id', $thread_id)
//        ->execute();
//      $query = db_insert('artesian_forum_thread_relation')->fields(array('thread_id', 'forum_id', 'current_forum'));
//      foreach ($values as $record) {
//        $query->values($record);
//      }
//      $query->execute();
//    }
//  }
}
