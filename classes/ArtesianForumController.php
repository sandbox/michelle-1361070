<?php

// @todo Not entirely sure what should be in the controllers, yet.

/**
 * @file
 */

class ArtesianForumController extends EntityAPIController {

  public function getAddForm($forum) {
    return drupal_get_form('artesian_forum_form', $forum);
  }

  public function getEditForm($forum) {
    return drupal_get_form('artesian_forum_form', $forum);
  }

  public function getDeleteForm($forum) {
    return drupal_get_form('artesian_forum_delete_form', $forum);
  }

  public function save($forum) {
    // Run the default entity save.
    $save_result = parent::save($forum);

    // Save the ancestry.
    // @todo Make sure the save was successful first.
    $this->saveAncestry($forum);

    return $save_result;
  }

  /**
   * Updates {artesian_forum_ancestor} with the current ancestry of this forum.
   *
   * This function removes the existing ancestry and replaces it with a new
   * set of records gathered from {artesian_forum}. It also updates this forum's
   * depth in {artesian_forum} by saving the entity.
   *
   * return
   *   @todo Should return FALSE if it failed.
   *
   */
  public function saveAncestry($forum) {
    // Remove current entries.
    db_delete('artesian_forum_ancestor')
      ->condition('forum_id', $forum->id())
      ->execute();

    // Get a list of ancestors.
    $ancestry = $forum->createAncestry();

    // Repopulate the ancestor table.
    $query = db_insert('artesian_forum_ancestor')
      ->fields(array('forum_id', 'ancestor_id', 'depth'));

    foreach ($ancestry as $depth => $ancestor) {
      $query->values(array(
        'forum_id' => $forum->id(),
        'ancestor_id' => $ancestor,
        'depth' => $depth)
        );
    }

    $query->execute();

    // Set the depth on the object for consistency.
    $forum->depth = $depth;

    // Update the depth in the entity's {artesian_forum} record.
    $query = db_update('artesian_forum');
    $query->fields(array('depth' => $depth))
    ->condition('forum_id', $forum->id())
    ->execute();

    return;
  }

}
