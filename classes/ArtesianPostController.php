<?php

/**
 * @file
 */

class ArtesianPostController extends EntityAPIController {

  public function create(array $values = array()) {
    $post = parent::create($values);

    // Ensure the parent id (post_id) has a value set.
    $this->parent_id = (empty($this->parent_id)) ? 0 : $this->parent_id;

    // Determine this post's position in the thread's hierarchy of posts.
    $post->position();

    // Get the IP being used to post.
    $post->ip = ip_address();

    return $post;
  }

  public function save($post) {
    // Set the changed and (if not already set) created times to the constant
    // that holds the time. REQUEST_TIME is used for performance reasons.
    $post->changed = REQUEST_TIME;
    $post->created = empty($post->created) ? REQUEST_TIME : $post->created;

    // Copy the author information from the Author object into the post.
    $post->copyFromAuthor();

    // Run the parent class code to save the post.
    $save_result = parent::save($post);

    // Have the thread this is posted to update its statistics. We do this
    // even on edit just to be safe because changing the published status
    // can affect this as well.
    $post->thread()->loadStatistics();
    $post->thread()->save();

    return $save_result;
  }
}
