<?php

/**
 * The ArtesianEntity class
 */
class ArtesianEntity extends Entity {

  /**
   * Returns TRUE if this is a newly created and unsaved entity.
   */
  public function isNew() {
    return !empty($this->is_new);
  }

  /**
   * Returns the title of this entity linked to the entity page.
   */
  public function linkedLabel() {
    return l($this->label(), $this->url());
  }

  /**
   * Returns the URL string
   */
  public function url() {
    $uri = $this->defaultUri();
    return $uri['path'];
  }

  /**
   * Returns the URL to view the entity.
   *
   * Does the same as url. Included for consistency with edit/delete.
   */
  public function viewURL() {
    return $this->url();
  }

  /**
   * Returns the URL to edit the entity.
   */
  public function editURL() {
    $url = $this->url();
    return $url . '/edit';
  }

  /**
   * Returns the URL to delete the entity.
   */
  public function deleteURL() {
    $url = $this->url();
    return $url . '/delete';
  }
}
