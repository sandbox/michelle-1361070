<?php

/**
 * The ArtesianThread entity class
 */
class ArtesianThread extends ArtesianEntity {
  // For debugging.
  public $instance_id;

  // Properties that map to fields in {artesian_thread}.
  public $thread_id;
  public $title;
  public $created;
  public $changed;
  public $published = 1;
  public $sticky;
  public $announcement;
  public $closed;
  public $attachments;
  public $author_id;
  public $author_display_id;
  public $author_display_name;
  public $last_post_id;
  public $last_post_time;
  public $last_post_author_id;
  public $last_post_author_display_id;
  public $last_post_author_display_name;
  public $post_count = 1;
  public $post_count_all = 1;
  public $view_count;

  // Post entity subtype used by this thread.
  //@todo This needs to be customizable per thread type.
  public $post_type = 'post';

  // Entity of the current forum context. May not be the only forum this thread
  // is posted in, but it is the one we are currently dealing with.
  public $forum;

  // Array of all forum relationships.
  public $forumList;

  // The actual object of the anchor post, loaded only when needed.
  protected $anchorPost;

  public function __construct(array $values = array(), $entityType = NULL) {
    // @todo: This is being used for temp code during development but can be removed for release.
    // Do the default entity constructing.
    $parent_result = parent::__construct($values, $entityType);

    return $parent_result;
  }

  /**
   * Returns the identifier of this thread.
   */
  public function id() {
    return $this->thread_id;
  }

  /**
   * Returns the title of the thread.
   */
  public function label() {
    return $this->title;
  }

  /**
   * Returns the URL to reply to the thread.
   */
  public function replyURL() {
    $url = $this->url();
    return $url . '/reply';
  }

  /**
   * Returns the post entity of the anchor post, creating it if necessary.
   */
  public function anchorPost($auto_load = TRUE, $auto_create = TRUE) {
    if (empty($this->anchorPost)) {
      if ($auto_load) {
        $this->loadAnchorPost();

        if (empty($this->anchorPost) && $auto_create) {
          $this->createAnchorPost();
        }
      }
    }

    return $this->anchorPost;
  }


// RENDERING ------------------------------------------------------------------\


  /**
   * Renders the thread.
   *
   * @param int $post_id
   *   (optional) The post ID to jump to.
   *
   * @return string
   */
  public function render($post_id = 0) {
    /* @todo
     * Use Views
     * Build the thread header
     * Add quick reply form as needed
     * Allow for getting a subtree for potential ajax loading
     * Use proper theming
     * Possibly load all the user objects at once?
     */

    $post_ids = $this->getPostTree();
    $posts = artesian_post_load_multiple($post_ids);

    $output = "<div>---Placeholder for thread header---</div>";

    foreach ($posts as $post) {
      $output .= $post->render();
    }

    $output .= '<div>' . l(t('Reply to thread'), $this->replyURL()) . '</div>';

    return $output;
  }

  /**
   * Renders a given post in the context of its thread.
   *
   * @todo Make this work.
   *
   * @param ArtesianPost $post
   *
   * @return string
   *
   */
  public function renderPost(ArtesianPost $post) {
    /* @todo
     * Use proper theming
     * Add rest of thread either in normal form or in titles-only threaded mode
     *  ...
     */
    return $this->render($post->id());
  }

  /**
   * Renders the thread in a stripped down format for use on reply post forms.
   *
   * @todo Make this work.
   *
   * @return string
   *
   */
  public function renderReview() {
    return $this->render();
  }



// FORMS ----------------------------------------------------------------------\

  public function getAddForm() {
    return drupal_get_form('artesian_thread_form', $this);
  }

  public function getEditForm() {
    return drupal_get_form('artesian_thread_form', $this);
  }

  public function getDeleteForm() {
    return drupal_get_form('artesian_thread_delete_form', $this);
  }


// UTILITY FUNCTIONS ----------------------------------------------------------\

  /**
   * Returns the page number where the given post is currently located.
   */
  public function getPostPage() {
    // @todo: Elaborate on hardcoded stub.
    return 1;
  }

  /**
   * Creates the anchor (first) post of the thread.
   *
   * @return ArtesianPost
   */
  protected function createAnchorPost() {
    $this->anchorPost = entity_create('artesian_post', array('thread_id' => $this->id(), 'type' => $this->post_type));
    $this->anchorPost->anchor = TRUE;
    return $this->anchorPost;
  }

  /**
   * Loads the anchor post of this thread into the anchorPost property.
   */
  protected function loadAnchorPost($force_reload = FALSE) {
    // If the anchor post is already loaded and we aren't forcing a reload, nothing to do.
    if (is_object($this->anchorPost) && !$force_reload) {
      return;
    }

    // Find the post marked for this thread and also marked as the anchor.
    $query = db_select('artesian_post', 'ap');
    $result = $query
      ->fields('ap', array('post_id'))
      ->condition('ap.thread_id', $this->thread_id)
      ->condition('ap.anchor', 1)
      ->execute();

    $post_id = $result->fetchField();

    if (!empty($post_id)) {
      $this->anchorPost = artesian_post_load($post_id);
      return $post_id;
    }
    else {
      // Unable to load post.
      return FALSE;
    }
  }

  /**
   * Returns the default URI.
   *
   * This function is overridden to use a custom path.
   */
  protected function defaultUri() {
    // @todo Make the default path a setting.
    return array('path' => 'forum/thread/' . $this->identifier());
  }

  /*
  * Returns an array of post IDs in the proper threaded sequence.
  *
  * $todo This will be modeled on comment_get_thread() but is using a simple
  * query for now.
  */
  public function getPostTree() {
    $query = db_select('artesian_post', 'ap');
    $query
      ->condition('ap.thread_id', $this->thread_id)
      ->fields('ap', array('post_id'));

    $post_ids = $query->execute()->fetchCol();

    return $post_ids;
  }


// STATISTICS -----------------------------------------------------------------\

  /**
   * Marks a thread and forum it is in as having been viewed.
   */
  public function markViewed() {
    global $user;
    $uid = $user->uid;
//@todo Broken
    return;

    if ($user->uid != 0) {
      // Remove current entries.
//      db_delete('artesian_forum_viewed')
//        ->condition('user_id', $uid)
//        ->condition('forum_id', $forum->id())
//        ->execute();

      // Remove current entry for this thread.
      db_delete('artesian_thread_viewed')
        ->condition('user_id', $uid)
        ->condition('thread_id', $this->id())
        ->execute();

      // Add a new entry.
      $query = db_insert('artesian_thread_viewed')
        ->fields(array('user_id', 'thread_id', 'viewed_on'))
        ->values($user->uid, $this->id(), REQUEST_TIME);

//      $query = db_insert('artesian_forum_viewed')
//        ->fields(array('user_id', 'forum_id', 'viewed_on'))
//        ->values($user->uid, $this->id(), REQUEST_TIME);
    }
  }

  /**
   * Loads the statistics for this thread from the database.
   */
  public function loadStatistics() {
    // Pull all the posts that go with this thread, newest first.
    $query = db_select('artesian_post', 'ap');
    $result = $query
      ->condition('ap.thread_id', $this->id())
      ->condition('ap.published', 1)
      ->orderBy('ap.created', 'DESC')
      ->fields('ap', array('post_id', 'created', 'author_id', 'author_display_id', 'author_display_name'))
      ->execute();
    $record = $result->fetchAssoc();

    // Fill in info about the last post.
    $this->last_post_id = $record['post_id'];
    $this->last_post_time = $record['created'];
    $this->last_post_author_id = $record['author_id'];
    $this->last_post_author_display_id = $record['author_display_id'];
    $this->last_post_author_display_name = $record['author_display_name'];

    // Get the published post count.
    $this->post_count = $query->countQuery()->execute()->fetchField();

    // Get a count of all posts on this thread, published or not.
    $query = db_select('artesian_post', 'ap');
    $this->post_count_all = $query
      ->condition('ap.thread_id', $this->thread_id)
      ->countQuery()
      ->execute()
      ->fetchField();
  }

  /**
   * Sets the last_post_id property.
   *
   * If a post ID is passed in, it will use that. If not, it will pull the info
   * from the database.
   *
   * @param int $post_id
   */
  public function setLastPostId($post_id = 0) {
    if (empty($post_id)) {
      $query = db_select('artesian_post', 'ap');
      $result = $query
        ->fields('ap', array('post_id'))
        ->condition('ap.thread_id', $this->thread_id)
        ->range(0, 1)
        ->orderBy('created', 'DESC')
        ->execute();
      $post_id = $result->fetchField();
    }

    $this->last_post_id = $post_id;
  }

  /**
   * Sets the statistics about the last post.
   *
   * This function will either use a passed in post id or the post id already
   * set on the thread. It will not search the database for the last post.
   * To load the stats based on what's in the database, use loadStatistics() .
   *
   * @param int $post_id
   */
  public function setLastPostStatistics($post_id = 0) {
    // If the last post ID is not already set or if the ID being passed in is
    // different, first (re)set the last post ID.
    if (empty($this->last_post_id) || (!$this->last_post_id == $post_id)) {
      $this->setLastPostId();
    }

    // Load the last post and use its info to fill our statistics.
    $last_post = artesian_post_load($this->last_post_id);
    $this->last_post_time = $last_post->created;
    $this->last_post_author_id = $last_post->author_id;
    $this->last_post_author_id = $last_post->author_display_id;
    $this->last_post_author_name = $last_post->author_display_name;
  }

  public function addForumRelation($forum) {
    // Create the relation object.
    $this->forumList[] = entity_create('artesian_forum_thread_relation', array(
      'type' => 'relation',
      'thread_id' => $this->id(),
      'forum_id' => $forum->id(),
      'current_forum' => $forum->id(),
    ));

  }
}
