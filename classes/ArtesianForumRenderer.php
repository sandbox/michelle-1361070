<?php

/**
 * Renders a forum.
 *
 * Default class for rendering a forum. Bundle types may each have their own
 * class to allow different types of forums to be rendered differently.
 *
 */
class ArtesianForumRenderer {

  /**
   * Gathers information from $forum and renders it.
   *
   * @param object $forum
   *   The forum object to be rendered.
   *
   * @return string
   *   The HTML markup from rendering the forum.
   */
  public function render($forum) {
    $child_forums = $this->renderChildren($forum, "Forums in " . $forum->label(), FALSE);

    $threads = $this->renderThreads($forum);

    $output = "<div>$child_forums</div><div>$threads</div>";

    return $output;
  }

  /**
   * Renders a simple list of child forums.
   *
   * @todo Use proper theming.
   */
  public function renderChildList($forum) {
    $query = db_select('artesian_forum', 'af')
      ->fields('af', array('forum_id', 'name'))
      ->condition('af.parent_id', $forum->id());
    $forum_list  = $query->execute()->fetchAll();

    $subforum_list = array();
    foreach ($forum_list AS $forum_info) {
      $subforum_list[] = l($forum_info->name, "$forum_info->forum_id" );
    }

    $output = '';
    if (!empty($subforum_list)) {
      $output = t('Subforums:');
      $output .= theme('item_list', array('items' => $subforum_list));
    }
    return $output;
  }

  /**
   * Displays this forum's child forums.
   *
   * @param string $caption
   *   Table caption / section header. Defaults to the forum name.
   * @param boolean $show_always
   *   If TRUE, the normal markup will be returned with a message that there
   *   are no child forums if none are found.
   *   If FALSE, an empty string will be returned if there are no children.
   *
   * @todo This is temporarily using theme_table() and needs to be replaced by
   *   a template file.
   *
   * @return string
   *   A string containing the HTML to display or empty if no child forums.
   */
  public function renderChildren($forum, $caption = NULL, $show_always = FALSE) {
    // Retrieve the children.
    $forum->loadDescendents();
    if (empty($forum->children)) {
      // Loads child forums and their children as well.
      $forum->loadChildren();
    }

    if (!empty($forum->children) || $show_always) {
      $table = array();
      $table['caption'] = (is_null($caption)) ? $forum->linkedLabel() : $caption;
      $table['header'] = array('Forum', 'Threads', 'Posts', 'Last Post');

      foreach ($forum->children as $child) {
        $child->loadAggregateCounts();
        $child->loadAggregateLastPost();

        $table['rows'][] = array(
          $child->linkedLabel() . '<br />' . $child->renderChildList(),
          $child->aggregateThreadCount,
          $child->aggregatePostCount,
          $child->aggregateLastPost,
        );
      }

      if (empty($table['rows'])) {
        $table['rows'][] = array(
          array('data' => 'No forums found', 'colspan' => 4),
        );
      }

      return theme('table', $table);
    }
    else {
      return '';
    }
  }

  /**
   * Renders the list of threads in this forum.
   *
   * @todo This will be using Views so only a temp function for now.
   */
  public function renderThreads($forum) {
    $html = l(t("New thread"), "forum/$forum->forum_id/add/thread");
    $header = array(
      array('data' => 'Title', 'field' => 'title', 'sort' => 'asc'),
      array('data' => 'Created', 'field' => 'created'),
      array('data' => 'Posts', 'field' => 'post_count'),
      array('data' => 'Views', 'field' => 'view_count'),
      array('data' => 'Last post', 'field' => 'last_post_id'),
    );

    $query = db_select('artesian_thread', 'at');
    $query->join('artesian_forum_thread_relation', 'aftr', 'at.thread_id = aftr.thread_id');
    $query->fields('at', array('thread_id', 'title', 'created', 'post_count', 'view_count', 'last_post_id'))
      ->condition('at.published', 1)
      ->condition('aftr.forum_id', $forum->id())
      ->extend('PagerDefault')
      ->limit(10)
      ->extend('TableSort')
      ->orderByHeader($header);

    $results = $query->execute();

    $rows = array();
    foreach ($results as $thread) {
      $rows[] = array(
        'data' => array(
          l($thread->title, "forum/thread/$thread->thread_id"),
          format_date($thread->created),
          $thread->post_count,
          $thread->view_count,
          $thread->last_post_id,
        )
      );
    }

    $html .= theme('table', array(
      'header' => $header,
      'rows' => $rows,
      'caption' => t('Forum threads'),
      'sticky' => TRUE,
      'empty' => t('No threads found'),
      )
    );

    $html .= theme('pager', array('tags' => array()));

    return ($html);

  }
}