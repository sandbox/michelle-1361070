<?php

/**
 * Page callback for adding a new thread.
 *
 * @param object $forum
 *   (optional) Forum to which to add the thread. Will show selection if NULL.
 * @param string $bundle_type
 *   (optional) Thread bundle type to create. Defaults to 'thread'.
 *
 * @return
 *   Form passed through drupal_get_form().
 */
function artesian_thread_add_page($forum = NULL, $bundle_type = 'thread') {
  // Create the thread object.
  $thread = entity_create('artesian_thread', array(
    'type' => $bundle_type,
    'forum' => $forum,
    ));

  return $thread->getAddForm();
}

/**
 * Page callback for editing a thread.
 *
 * @param $thread
 *   The thread object to edit.
 *
 * @return
 *   Form passed through drupal_get_form().
 */
function artesian_thread_edit_page($thread) {
  // Load the anchor post as we want to edit that at the same time.
  $thread->anchorPost();

  return $thread->getEditForm();
}

/**
 * Page callback for deleting a thread.
 *
 * @param $thread
 *   The thread object to delete.
 *
 * @return
 *   Form passed through drupal_get_form().
 */
function artesian_thread_delete_page($thread) {
  return $thread->getDeleteForm();
}

/**
 * Defines form for creating, editing or deleting a thread.
 *
 * @param $form
 * @param $form_state
 * @param $thread
 *   The thread entity, either new for add or existing for edit/delete.
 */
function artesian_thread_form($form, &$form_state, $thread) {
  if (!$form_state['rebuild']) {
    // Use the "Post" form as a base for the "Thread" form.
    $form = artesian_post_form($form, $form_state, $thread->anchorPost());
  }

  // Save the thread entity to the form state so we can access it from other
  // form processing functions. If it already exists, grab it.
  // Note: We add the extra "holder" level because otherwise
  // entity_form_submit_build_entity will try and overwrite the protected
  // "thread" property of the post entity.
  if (isset($form_state['holder']['thread'])) {
    $thread = $form_state['holder']['thread'];
  }
  else {
    $form_state['holder']['thread'] = $thread;
  }

  // Assign the form class for theming.
  $form['#attributes']['class'][] = 'thread-form';
  if (!empty($thread->type)) {
    $form['#attributes']['class'][] = 'thread-' . $thread->type . '-form';
  }

  // Title is required on threads.
  $form['title']['#required'] = TRUE;

  /* @todo Make UI for selecting forum.
  $form['thread_options']['forums'] = array(
    '#type' => 'select',
    '#title' => t('Forums'),
    '#options' => artesian_forum_option_list(),
    '#default_value' => $thread->forums,
    '#weight' => 40,
    '#required' => FALSE,
    '#multiple' => TRUE,
  );
   */

  // Add the field related thread form elements.
  field_attach_form('artesian_thread', $thread, $form, $form_state);

  // Add the buttons.
  $form['buttons'] = array();
  $form['buttons']['#weight'] = 100;
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Post thread'),
    '#weight' => 5,
    '#submit' => array('artesian_thread_form_submit'),
  );

  $form['buttons']['delete'] = array(
    '#access' => user_access('delete threads'),
    '#type' => 'submit',
    '#value' => t('Delete thread'),
    '#weight' => 15,
    '#submit' => array('artesian_thread_form_delete_submit'),
  );

  return $form;
}

/**
 * Validation for thread form
 */
function artesian_thread_form_validate($form, &$form_state) {
  // @TODO: Actually check that all these validations are passing.
  // Retrieve the stored entities.
  $thread = $form_state['holder']['thread'];
  $post = $thread->anchorPost();

  // Let the FieldAPI do its validation.
  field_attach_form_validate('artesian_thread', $thread, $form, $form_state);
  field_attach_form_validate('artesian_post', $post, $form, $form_state);

  // Assign the changed entities back into the array.
  //$thread->anchorPost = $post;
  $form_state['holder']['thread'] = $thread;
}

/**
 * Submit function for thread form
 */
function artesian_thread_form_submit($form, &$form_state) {
  $thread = $form_state['holder']['thread'];

  // Run the posts's code to prepare for submission since we are by-passing the
  // post's submit function.
  artesian_post_form_prepare_submit($form, $form_state);
  $post = $form_state['post'];

  // This core function copies all the form values into the entity objects and
  // also handles submitting the field API field data. Note that this will fail
  // to reach values in fieldsets if the fieldset has #tree = TRUE set.
  entity_form_submit_build_entity('artesian_thread', $thread, $form, $form_state);
  entity_form_submit_build_entity('artesian_post', $post, $form, $form_state);

  // Copy the author information from the post.
  $thread->author_id = $post->author()->authorID;
  $thread->author_display_id = $post->author()->authorDisplayID;
  $thread->author_display_name = $post->author()->authorDisplayName;

  // Save the thread.
  $save_result = $thread->save();

  // Let the anchor post know its thread's id and then save it.
  $thread->anchorPost()->thread_id = $thread->id();
  $thread->anchorPost()->save();

  $form_state['redirect'] = $thread->url();



  /* @todo This needs to be redone since we moved the thread/post saving.
  // Write the results to Watchdog.
  $watchdog_args = array('@type' => $thread->entityType(), '%title' => $thread->label());
  $t_args = array('@type' => $thread->entityType(), '%title' => $thread->title);

  if ($thread_result == SAVED_NEW) {
    watchdog('artesian', '@type: added %title.', $watchdog_args, WATCHDOG_NOTICE, $thread->URL());
    drupal_set_message(t('@type %title has been created.', $t_args));
  }
  elseif ($thread_result == SAVED_UPDATED) {
    watchdog('artesian', '@type: updated %title.', $watchdog_args, WATCHDOG_NOTICE, $thread->URL());
    drupal_set_message(t('@type %title has been updated.', $t_args));
  }
  else {
    // In the unlikely case something went wrong on save, the thread will be
    // rebuilt and thread form redisplayed the same way as in preview.
    drupal_set_message(t('The thread could not be saved.'), 'error');
    $form_state['rebuild'] = TRUE;
  }
  */
}

/**
 * Button submit function: handle the 'Delete' button on the thread form.
 */
function artesian_thread_form_delete_submit($form, &$form_state) {
  $destination = array();
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  $thread = $form_state['holder']['thread'];

  $form_state['redirect'] = array($thread->deleteURL(), array('query' => $destination));
}

/**
 * Form constructor for the confirmation form for deleting a thread.
 *
 * @param $thread
 *   The thread object to delete.
 *
 * @see artesian_thread_delete_page()
 * @see confirm_form()
 */
function artesian_thread_delete_form($form, &$form_state, $thread) {
  $form_state['holder']['thread'] = $thread;
  $form['#submit'][] = 'artesian_thread_delete_form_submit';
  $question = t('Are you sure you want to delete this thread?');

  $form = confirm_form($form, filter_xss($question), '<front>', t('This action cannot be undone.'), t('Delete'), t('Cancel'));

  return $form;
}

/**
 * Execute thread deletion
 */
function artesian_thread_delete_form_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $thread = $form_state['holder']['thread'];
    $thread->delete();
    watchdog('thread', '@type: deleted %title.', array('@type' => $thread->typeName(), '%title' => $thread->name));
    drupal_set_message(t('@type %title has been deleted.', array('@type' => $thread->typeName(), '%title' => $thread->name)));
  }

  $form_state['redirect'] = '<front>';
}

