<?php

/**
 * Page callback for adding a new forum.
 *
 * @param string $bundle_type
 *   Forum bundle type to create. Defaults to Forum.
 *
 * @return
 *   Form passed through drupal_get_form().
 */
function artesian_forum_add_page($bundle_type = 'forum') {

  // Create the forum object.
  $forum = entity_create('artesian_forum', array('type' => $bundle_type));

  return $forum->getAddForm();
}

/**
 * Page callback for editing a forum.
 *
 * @param $forum
 *   The forum object to edit.
 *
 * @return
 *   Form passed through drupal_get_form().
 */
function artesian_forum_edit_page($forum) {
  return $forum->getEditForm();
}

/**
 * Page callback for deleting a forum.
 *
 * @param $forum
 *   The forum object to delete.
 *
 * @return
 *   Form passed through drupal_get_form().
 */
function artesian_forum_delete_page($forum) {
  return $forum->getDeleteForm();
}

/**
 * Generate the form for creating, editing or deleting a forum.
 *
 * @param $form
 * @param $form_state
 * @param $forum
 *   The forum entity, either new for add or existing for edit/delete.
 */
function artesian_forum_form($form, &$form_state, $forum = NULL) {

  // During initial form build, add the Forum entity to the form state for use
  // during form building and processing. During a rebuild, use what is in the
  // form state.
  if (!isset($form_state['forum'])) {
    $form_state['forum'] = $forum;
  }
  else {
    $forum = $form_state['forum'];
  }

  // Assign the form class for theming.
  $form['#attributes']['class'][] = 'forum-form';
  if (!empty($forum->type)) {
    $form['#attributes']['class'][] = 'forum-' . $forum->type . '-form';
  }

  // Basic forum information.
  // These elements are just values so they are not even sent to the client.
  foreach (array('forum_id', 'owner_id', 'type') as $key) {
    $form[$key] = array(
      '#type' => 'value',
      '#value' => isset($forum->$key) ? $forum->$key : NULL,
    );
  }

  // Name of the forum.
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $forum->label(),
    '#weight' => 1,
    '#required' => TRUE,
  );

  // Parent forum.
  $form['parent_id'] = array(
    '#type' => 'select',
    '#title' => t('Parent forum'),
    '#options' => artesian_forum_option_list(),
    '#default_value' => $forum->parent_id,
    '#weight' => 3,
    '#required' => FALSE,
  );

  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Display order (weight)'),
    '#default_value' => $forum->weight,
    '#weight' => 4,
    '#required' => FALSE,
  );

  // Add the buttons.
  $form['buttons'] = array();
  $form['buttons']['#weight'] = 100;
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save forum'),
    '#weight' => 5,
    '#submit' => array('artesian_forum_form_submit'),
  );

  if (!empty($forum->forum_id)) {
    $form['buttons']['delete'] = array(
      '#access' => user_access('delete forums'),
      '#type' => 'submit',
      '#value' => t('Delete forum'),
      '#weight' => 15,
      '#submit' => array('artesian_forum_form_delete_submit'),
    );
  }

  field_attach_form('artesian_forum', $forum, $form, $form_state);

  return $form;
}

/**
 * Validation for forum form
 */
function artesian_forum_form_validate($form, &$form_state) {
  // @TODO: Actually check that all these validations are passing.

  // Retrieve the stored entity.
  $forum = $form_state['forum'];

  // Let the FieldAPI do its validation.
  field_attach_form_validate('forum', $forum, $form, $form_state);

  // Assign the changed Forum entity back into the array.
  $form_state['values']['forum'] = $forum;
}

/**
 * Submit function for forum form
 */
function artesian_forum_form_submit($form, &$form_state) {
  // Retrieve the forum entity object.
  $forum = $form_state['forum'];

  // This core function copies all the form values into the entity objects and
  // also handles submitting the field API field data.
  entity_form_submit_build_entity('forum', $forum, $form, $form_state);

  // Save the forum entity.
  $result = $forum->save();

  $watchdog_args = array('@type' => $forum->entityType(), '%title' => $forum->label());
  $t_args = array('@type' => $forum->entityType(), '%title' => $forum->label());

  if ($result == SAVED_NEW) {
    watchdog('artesian', '@type: added %title.', $watchdog_args, WATCHDOG_NOTICE, $forum->URL());
    drupal_set_message(t('@type %title has been created.', $t_args));
  }
  elseif ($result == SAVED_UPDATED) {
    watchdog('artesian', '@type: updated %title.', $watchdog_args, WATCHDOG_NOTICE, $forum->URL());
    drupal_set_message(t('@type %title has been updated.', $t_args));
  }
  else {
    // In the unlikely case something went wrong on save, the forum will be
    // rebuilt and forum form redisplayed the same way as in preview.
    drupal_set_message(t('The forum could not be saved.'), 'error');
    $form_state['rebuild'] = TRUE;
  }
}

/**
 * Button submit function: handle the 'Delete' button on the forum form.
 */
function artesian_forum_form_delete_submit($form, &$form_state) {
  $destination = array();
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  $forum = $form_state['values']['forum'];

  $form_state['redirect'] = array($forum->deleteURL(), array('query' => $destination));
}

/**
 * Form constructor for the confirmation form for deleting a forum.
 *
 * @param $forum
 *   The forum object to delete.
 *
 * @see artesian_forum_delete_page()
 * @see confirm_form()
 */
function artesian_forum_delete_form($form, &$form_state, $forum) {
  $form_state['forum'] = $forum;
  $form['#submit'][] = 'artesian_forum_delete_form_submit';
  $question = t('Are you sure you want to delete this forum?');

  $form = confirm_form($form, filter_xss($question), '<front>', t('This action cannot be undone.'), t('Delete'), t('Cancel'));

  return $form;
}

/**
 * Execute forum deletion
 */
function artesian_forum_delete_form_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $forum = $form_state['forum'];
    $forum->delete();
    watchdog('forum', '@type: deleted %title.', array('@type' => $forum->entityType(), '%title' => $forum->label()));
    drupal_set_message(t('@type %title has been deleted.', array('@type' => $forum->entityType(), '%title' => $forum->label())));
  }

  $form_state['redirect'] = '<front>';
}
