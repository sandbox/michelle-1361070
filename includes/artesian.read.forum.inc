<?php

/**
 * Displays the forum overview page.
 *
 * @return
 *   The HTML of the page requested.
 */
function artesian_forum_overview_page() {
  // Gather the root containers.
  $containers = artesian_forum_load_by_depth(0, 0);

  // @todo Replace this with a proper theme function.
  $output = '';
  foreach ($containers as $container) {
    $output .= $container->renderChildren(NULL, TRUE);
  }

  return $output;
}

function artesian_forum_list($start_depth = 0, $end_depth = 3) {
  $forums = artesian_forum_load_by_depth($start_depth, $end_depth);

  //dsm($forums);
  return $forums;


}

/**
 * Displays an individual forum page.
 *
 * @param ArtesianForum $forum
 *   Forum object to display.
 *
 * @return
 *   The HTML of the page requested.
 */
function artesian_forum_view_page(ArtesianForum $forum) {
  drupal_set_title($forum->label());

  return $forum->render();
}

/**
 * Loads the requested forum object by ID.
 *
 * @param $forum_id
 *   Integer specifying the forum entity id.
 * @param $reset
 *   A boolean indicating that the internal cache should be reset. Defaults
 *   to FALSE.
 *
 * @return
 *   A fully-loaded forum object or FALSE if it cannot be loaded.
 *
 * @see artesian_forum_load_multiple()
 */
function artesian_forum_load($forum_id = NULL, $reset = FALSE) {
  $forum = artesian_forum_load_multiple(array($forum_id), array(), $reset);

  return reset($forum);
}

/**
 * Loads multiple forum objects.
 *
 * @param $forum_ids
 *   An array of forum forum_ids, or FALSE to load all forums.
 * @param $conditions
 *   An array of conditions to match against the {artesian_forum} table.
 * @param $reset
 *   A boolean indicating that the internal cache should be reset. Defaults
 *   to FALSE.
 *
 * @return
 *   An array of forum objects indexed by their ids. When no results are found,
 *   an empty array is returned.
 *
 * @see entity_load()
 */
function artesian_forum_load_multiple($forum_ids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('artesian_forum', $forum_ids, $conditions, $reset);
}

/**
 * Loads all forum objects that are at a given depth in the hierarchy.
 *
 * @param
 *   int $depth
 *
 * @return
 *   An array of forum entities or an empty array if none exist.
 */
function artesian_forum_load_by_depth($start_depth = 0, $end_depth = NULL) {
  $end_depth =  (is_null($end_depth)) ? $start_depth + 2 : $end_depth;

  $query = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'artesian_forum')
    ->propertyCondition('depth', array($start_depth, $end_depth), 'BETWEEN')
    ->propertyOrderBy('depth')
    ->propertyOrderBy('parent_id')
    ->propertyOrderBy('weight');

  $result = $query->execute();

  if (count($result) > 0) {
    $forum_ids = array_keys($result['artesian_forum']);
    $forums = entity_load('artesian_forum', $forum_ids);
  }
  else {
    $forums = array();
  }

  return $forums;
}

/**
 *
 * @param int $forum_id
 *   ID of forum to begin with.
 * @param int $depth
 *   How many levels deep to go. 0 for unlimited.
 *
 * @return array
 *   List of forums in hierarchical order.
 *
 * @todo This doesn't really work, yet.
 */
function artesian_forum_tree($forum_id = 0, $depth = 0) {
  $tree = array();
  $query = db_select('artesian_forum', 'af');
  $query->fields('af', array('forum_id', 'name', 'parent_id', 'depth'));
  $result = $query->execute();

  foreach ($result as $forum) {
    $tree[$forum->forum_id] = array(
      'name' => $forum->name,
      'parent_id' => $forum->parent_id,
      'depth' => $forum->depth,
      );
  }

  return $tree;

}

// @todo Needs work.
function artesian_forum_option_list() {
  $options = array(0 => 'Root');
  $tree = artesian_forum_tree();
  foreach ($tree as $forum_id => $forum_leaf) {
    $options[$forum_id] = $forum_leaf['name'];
  }

  return $options;
}