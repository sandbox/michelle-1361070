<?php

function artesian_admin_structure_page() {
  $output = t('<p>Manage the Artesian Forum entities.</p>');

  return $output;
}

function artesian_forum_admin_list_page() {
  $output = theme('item_list', array(
    'forum' => 'Forum',
    'forum-container' => 'Forum container',
    'link-forum' => 'Link forum'
    )
  );

  return $output;
}

function artesian_forum_admin_page() {
  $output = '<p>The "forum" entity is used for normal forums. There are no
    settings at this time. You can add fields as needed.</p>';

  return $output;
}

function artesian_forum_container_admin_page() {
  $output = '<p>The "forum container" entity holds forums. There are no
    settings at this time. You can add fields as needed.</p>';

  return $output;
}

function artesian_link_forum_admin_page() {
  $output = '<p>The "link forum" entity is a special type of forum that is
    simply a link to elsewhere. There are no settings at this time. You can add
    fields as needed.</p>';

  return $output;
}

function artesian_thread_admin_page() {
  $output = '<p>The "thread" entity holds posts and meta data about the
    discussion thread. There are no settings at this time. You can add fields as
    needed.</p>';

  return $output;
}

function artesian_post_admin_page() {
  $output = '<p>The "post" entity is a single entry in the discussion thread.
    There are no settings at this time. You can add fields as needed.</p>';

  return $output;
}

