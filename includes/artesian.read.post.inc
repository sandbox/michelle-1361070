<?php

/**
 * Displays an individual post page.
 *
 * @param ArtesianThread $thread
 *   Thread object post belongs to.
 * @param ArtesianPost $post
 *   Post object to display.
 *
 * @return
 *   The HTML of the page requested.
 */
function artesian_post_view_page(ArtesianThread $thread, ArtesianPost $post) {
  drupal_set_title($post->label());

  // Ask the thread to render the post in context of the thread. We don't want
  // to be showing the post as a separate entity without the thread information.
  return $thread->renderPost($post);
}

/**
 * Fetches a post object.
 *
 * Make sure that the wildcard you choose in the post entity definition fits
 * the function name here. hook_menu() uses this function whenever the wildcard
 * appears in the URL.
 *
 * @param $pid
 *   Integer specifying the post entity id.
 * @param $reset
 *   A boolean indicating that the internal cache should be reset. Defaults
 *   to FALSE.
 *
 * @return
 *   A fully-loaded post object or FALSE if it cannot be loaded.
 *
 * @see post_load_multiple()
 */
function artesian_post_load($pid = NULL, $reset = FALSE) {
  $pids = (isset($pid) ? array($pid) : array());
  $post = artesian_post_load_multiple($pids, $reset);
  return $post ? reset($post) : FALSE;
}


/**
 * Loads multiple post objects.
 *
 * @param $pids
 *   An array of post pids, or FALSE to load all posts.
 * @param $conditions
 *   (deprecated) An associative array of conditions on the base table, where
 *   the keys are the database fields and the values are the values those
 *   fields must have. Instead, it is preferable to use EntityFieldQuery to
 *   retrieve a list of entity IDs loadable by this function.
 * @param $reset
 *   A boolean indicating that the internal cache should be reset. Defaults
 *   to FALSE.
 *
 * @return
 *   An array of post objects indexed by their ids. When no results are found,
 *   an empty array is returned.
 *
 * @see entity_load()
 */
function artesian_post_load_multiple($pids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('artesian_post', $pids, $conditions, $reset);
}

/**
 * Redirects post links to the correct page depending on post settings.
 *
 * Since posts are paged there is no way to guarantee which page a post
 * appears on. post paging and threading settings may be changed at any time.
 * With threaded posts, an individual post may move between pages as
 * posts can be added either before or after it in the overall discussion.
 * Therefore we use a central routing function for post links, which
 * calculates the page number based on current post settings and returns
 * the full thread with the pager set dynamically.
 *
 * @param $pid
 *   A post identifier.
 * @return
 *   The thread set to the page on which the post appears.
 */
function artesian_post_permalink($pid) {
  if (($post = artesian_post_load($pid)) && ($thread = artesian_thread_load($post->thread_id))) {

    // Find the current display page for this post.
    $page = $thread->getDisplayPage($post->pid);

    // Set $_GET['q'] and $_GET['page'] ourselves so that the thread callback
    // behaves as it would when visiting the page directly.
    $_GET['q'] = 'forum/thread/' . $thread->thread_id;
    $_GET['page'] = $page;

    // Return the thread view, this will show the correct post in context.
    return menu_execute_active_handler('forum/thread/' . $thread->thread_id, FALSE);
  }
  drupal_not_found();
}
