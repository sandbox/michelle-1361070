<?php
/**
 * Create the option form for generating the sample forum.
 *
 * @param $form
 * @param $form_state
 *
 * @return array $form
 */
function artesian_sample_forum_form($form, &$form_state) {
  // @todo Change the default back to 'NO!' before releasing.
  $form['empty_forum'] = array(
    '#type' => 'textfield',
    '#title' => t('Delete all existing forum content?'),
    '#description' => t("Type 'YES!' into this box to wipe out the forum before generating the sample. Use with care! This is irreversable!"),
    '#default_value' => 'YES!',
  );

  // Submit button
  $form['buttons'] = array();
  $form['buttons']['#weight'] = 100;
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Build forum'),
    '#weight' => 10,
    '#submit' => array('artesian_sample_forum_form_submit'),
  );

  return $form;
}

/**
 * Submit function for sample forum options form.
 *
 * @param $form
 * @param $form_state
 */
function artesian_sample_forum_form_submit($form, &$form_state) {
  if (!empty($form_state['values']['empty_forum']) && ($form_state['values']['empty_forum'] == 'YES!')) {
    artesian_sample_forum_empty_forums();
  }

  artesian_sample_forum_generate();

  drupal_set_message(t('Forum generated.'));

  $form_state['redirect'] = 'forum';

  return;
}

/**
 * Wipe out the existing forum.
 */
function artesian_sample_forum_empty_forums() {
  // @todo Need to delete field data, too.
  db_delete('artesian_forum')->execute();
  db_delete('artesian_thread')->execute();
  db_delete('artesian_post')->execute();
  db_delete('artesian_forum_ancestor')->execute();
  db_delete('artesian_forum_thread_relation')->execute();
  db_delete('field_data_artesian_forum_description')->execute();
  db_delete('field_revision_artesian_forum_description')->execute();
  db_delete('field_data_artesian_post_message')->execute();
  db_delete('field_revision_artesian_post_message')->execute();

  drupal_set_message(t('Forum emptied.'));

  return;
}

function artesian_quick_create_forum($name, $type = 'forum', $parent_id = 0, $description = '') {
  // Create the forum.
  $forum = entity_create('artesian_forum', array('type' => $type));

  // Set the non-field-api fields.
  $forum->name = $name;
  $forum->parent_id = $parent_id;

  $forum->set('description', $description);

  // Save it.
  entity_save('artesian_forum', $forum);

  // Send back the new forum.
  return $forum;
}

function artesian_quick_create_thread($forums, $title, $message, $type = 'thread') {
  // Create the thread.
  $thread = artesian_create_thread_with_anchor($forums, $type);

  // Set the non-field-api fields.
  $thread->title = $title;

  // Set the field api fields.
  $thread->anchorPost()->set('message', $message);

  // Save it.
  artesian_save_thread_with_anchor($thread);

  // Send back the new thread.
  return $thread;
}

function artesian_quick_create_post($thread_id, $message, $title = '', $type = 'post') {
  // Create the post.
  $post = entity_create('artesian_post', array('type' => $type, 'thread_id' => $thread_id));

  // Set the title in case one was passed in.
  $post->title= $title;

  $post->set('message', $message);

  // Save it.
  entity_save('artesian_post', $post);

  // Send back the new forum.
  return $post;

}

function artesian_sample_forum_generate() {
  // Create a container.
  $forum = artesian_quick_create_forum('The Universe', 'forum_container', 0, 'The whole shebang made from the big bang');

  // Grab the ID for use with its children.
  $container_id = $forum->id();

  // Create the forums.
  $forum_list = array();
  $forum_list[] = array(
    'name' => 'Gallifrey',
    'description' => 'Home planet of the Time Lords',
    'children' => array('Citidel', 'Academy', 'Panopticon'),
  );
  $forum_list[] = array(
    'name' => 'Earth',
    'description' => 'You are here',
    'children' => array('U.N.I.T. HQ', 'Cardiff', 'Lake Silencio'),
  );
  $forum_list[] = array(
    'name' => 'TARDIS',
    'description' => 'Time and Relative Dimensions in Space',
    'children' => array('Console room', 'Zero room', 'Library'),
  );

  foreach ($forum_list as $forum_info) {
    $forum = artesian_quick_create_forum($forum_info['name'], 'forum', $container_id, $forum_info['description']);
    $forum_id = $forum->id();

    // Create some threads.
    for ($t=1; $t<=5; $t++) {
      $title = 'Thread #' . $t . ' in ' . $forum_info['name'];
      $text = artesian_sample_forum_filler();
      $thread = artesian_quick_create_thread(array($forum_id => $forum_id), $title, $text);

      for ($p=1; $p<=5; $p++) {
        $title = 'Post #' . $p . ' in ' . $thread->title;
        $text = artesian_sample_forum_filler();
        artesian_quick_create_post($thread->id(), $text, $title);
      }
    }

    // Create subforums.
    foreach ($forum_info['children'] AS $forum_name) {
      $forum = artesian_quick_create_forum($forum_name, 'forum', $forum_id);
      $subforum_id = $forum->id();

      // Create some threads.
      for ($t=1; $t<=3; $t++) {
        $title = 'Thread #' . $t . ' in ' . $forum_name;
        $text = artesian_sample_forum_filler();
        $thread = artesian_quick_create_thread(array($subforum_id => $subforum_id), $title, $text);

        for ($p=1; $p<=3; $p++) {
          $title = 'Post #' . $p . ' in ' . $thread->title;
          $text = artesian_sample_forum_filler();
          artesian_quick_create_post($thread->id(), $text, $title);
        }
      }
    }
  }

  // Create another container.
  $forum = artesian_quick_create_forum('E-Space', 'forum_container', 0, 'A place to vent our entropy');

  // Grab the ID for use with its children.
  $container_id = $forum->id();

  // Create the forums.
  $forum_list = array();
  $forum_list[] = array(
    'name' => 'Alzarius',
    'description' => 'Home to the crashed starliner',
    'children' => array('Starliner', 'Marsh', 'Cave'),
  );
  $forum_list[] = array(
    'name' => 'Terradon',
    'description' => 'Where the starliner came from',
    'children' => array(),
  );
  $forum_list[] = array(
    'name' => "Warrior's Gate",
    'description' => 'Where Romana stayed behind',
    'children' => array(),
  );

  foreach ($forum_list as $forum_info) {
    $forum = artesian_quick_create_forum($forum_info['name'], 'forum', $container_id, $forum_info['description']);
    $forum_id = $forum->id();

    // Create subforums.
    foreach ($forum_info['children'] AS $forum_name) {
      $forum = artesian_quick_create_forum($forum_name, 'forum', $forum_id);
    }
  }

  return 'forum generated';
}

function artesian_sample_forum_filler() {
  $filler = array();
  $filler[] = "There are worlds out there where the sky is burning, and the sea is asleep, and the rivers dream. People made of smoke and cities made of song. Somewhere there's danger, somewhere there's injustice, somewhere else the tea's getting cold. Come on, Ace; we've got work to do!";
  $filler[] = "There was a war. A Time War. The Last Great Time War. My people fought a race called the Daleks, for the sake of all creation. And they lost. We lost. Everyone lost. They're all gone now. My family. My friends. Even that sky. [reminiscent] Oh, you should have seen it! That old planet... The second sun would rise in the south, and the mountains would shine. The leaves on the trees were silver, when they caught the light, every morning it looked like a forest on fire. When the autumn came, a brilliant glow though the branches...";
  $filler[] = "I'm old enough to know that a longer life isn't always a better one. In the end, you just get tired; tired of the struggle, tired of losing everyone that matters to you, tired of watching everything you love turn to dust. If you live long enough, Lazarus, the only certainty left is that you'll end up alone.";
  $filler[] = "People assume that time is a strict progression of cause to effect... but actually, from a non-linear, non-subjective viewpoint, it's more like a big ball of wibbly-wobbly... timey-wimey... stuff.";
  $filler[] = "Fascinating race, the Weeping Angels. The only psychopaths in the universe to kill you nicely. No mess, no fuss. They just zap you into the past and let you live to death. The rest of your life is used up and blown away in the blink of an eye. You die in the past, and in the present they consume the energy of all the days you might have had, all your stolen moments. They're creatures of the abstract. They live off potential energy.";
  $filler[] = "Well, perfect to look at, maybe. And it was, it was beautiful. They used to call it the Shining World of the Seven Systems. And on the continent of Wild Endeavour, in the mountains of Solace and Solitude, there stood the Citadel of the Time Lords. The oldest and most mighty race in the universe. Looking down on the galaxies below, sworn never to interfere, only to watch. Children of Gallifrey were taken from their families at the age of eight, to enter the Academy. Some say that's where it all began, when he was a child. That's when the Master saw eternity. As a novice, he was taken for initiation. He stood in front of the Untempered Schism. It's a gap in the fabric of reality through which could be seen the whole of the vortex. We stand there, eight years old, staring at the raw power of Time and Space, just a child. Some would be inspired. Some would run away. And some would go mad. ";
  $filler[] = "I travelled across the world. From the ruins of New York, to the fusion mills of China, right across the radiation pits of Europe. And everywhere I went I saw people just like you, living as slaves! But if Martha Jones became a legend then that's wrong, because my name isn't important. There's someone else. The man who sent me out there, the man who told me to walk the Earth. And his name is the Doctor. He has saved your lives so many times and you never even knew he was there. He never stops. He never stays. He never asks to be thanked. But I've seen him, I know him... I love him... And I know what he can do.";

  return $filler[array_rand($filler)];
}