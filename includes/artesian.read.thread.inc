<?php

/**
 * Displays an individual thread page.
 *
 * @param ArtesianThread $thread
 *   Thread object to display.
 *
 * @return
 *   The HTML of the page requested.
 */
function artesian_thread_view_page(ArtesianThread $thread) {
  drupal_set_title($thread->label());

  $thread->markViewed();

  return $thread->render();
}

/**
 * Loads the requested thread object by ID.
 *
 * @param $thread_id
 *   Integer specifying the thread entity id.
 * @param $reset
 *   A boolean indicating that the internal cache should be reset. Defaults
 *   to FALSE.
 *
 * @return
 *   A fully-loaded thread object or FALSE if it cannot be loaded.
 *
 * @see artesian_thread_load_multiple()
 */
function artesian_thread_load($thread_id = NULL, $reset = FALSE) {
  $thread_ids = (isset($thread_id) ? array($thread_id) : array());
  $thread = artesian_thread_load_multiple($thread_ids, $reset);
  return $thread ? reset($thread) : FALSE;
}

/**
 * Loads multiple thread objects.
 *
 * @param $thread_ids
 *   An array of thread thread_ids, or FALSE to load all threads.
 * @param $conditions
 *   (deprecated) An associative array of conditions on the base table, where
 *   the keys are the database fields and the values are the values those
 *   fields must have. Instead, it is preferable to use EntityFieldQuery to
 *   retrieve a list of entity IDs loadable by this function.
 * @param $reset
 *   A boolean indicating that the internal cache should be reset. Defaults
 *   to FALSE.
 *
 * @return
 *   An array of thread objects indexed by their ids. When no results are found,
 *   an empty array is returned.
 *
 * @see entity_load()
 */
function artesian_thread_load_multiple($thread_ids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('artesian_thread', $thread_ids, $conditions, $reset);
}

