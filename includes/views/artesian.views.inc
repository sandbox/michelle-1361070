<?php

/**
 * @file
 * Views integration for Artesian.
 */

/**
 * Implementation of hook_views_plugins
 */
function artesian_views_plugins() {
  $path = drupal_get_path('module', 'artesian') . '/includes/views';
  $theme_path = drupal_get_path('module', 'artesian') . '/includes/views';
  $plugins = array();
  $plugins['style']['artesian_thread'] = array(
    'path' => $path,
    'title' => t('Artesian thread'),
    'help' => t('Displays a thread.'),
    'handler' => 'artesian_plugin_style_thread',
    'theme' => 'artesian_view_thread',
    'theme path' => $theme_path,
    'uses row plugin' => TRUE,
    'uses row class' => TRUE,
    'uses grouping' => TRUE,
    'uses options' => FALSE,
    'type' => 'normal',
   );

  $plugins['row']['artesian_post'] = array(
    'path' => $path,
    'title' => t('Artesian post'),
    'help' => t('Displays a single post as part of a thread.'),
    'handler' => 'artesian_plugin_row_post',
    'theme' => 'artesian_view_post',
    'theme path' => $theme_path,
    'uses fields' => TRUE,
    'uses options' => FALSE,
    'type' => 'normal',
  );

  return $plugins;
}
