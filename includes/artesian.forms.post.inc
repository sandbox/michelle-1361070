<?php
/**
 * Page callback for adding a new post.
 *
 * @param object $thread
 *   Thread in which to add the post. Drupal's menu system takes care of making
 *   sure this is a valid thread so no need to check.
 *
 * @return
 *   Form passed through drupal_get_form().
 *
 */
function artesian_post_add_page($thread, $parent_id = 0) {
  // Create the post object.
  $post = entity_create('artesian_post', array(
    'type' => $thread->post_type,
    'thread_id' => $thread->id(),
    'parent_id' => $parent_id,
    ));

  return $post->getAddForm($thread);
}

/**
 * Page callback for editing a post.
 *
 * @param $post
 *   The post object to edit.
 *
 * @return
 *   Form passed through drupal_get_form().
 */
function artesian_post_edit_page($thread, $post) {
  return $post->getEditForm($thread);
}

/**
 * Page callback for deleting a post.
 *
 * @param $post
 *   The post object to delete.
 *
 * @return
 *   Form passed through drupal_get_form().
 */
function artesian_post_delete_page($thread, $post) {
  return $post->getDeleteForm($thread);
}

/**
 * Defines form for creating, editing or deleting a post.
 *
 * @param $post
 *   The post object to edit or an empty post object if new.
 * @param $thread
 *   The thread object the post is a part of.
 *
 * @see artesian_post_page()
 * @see artesian_post_edit_page()
 * @see artesian_post_add_form_validate()
 * @see artesian_post_add_form_submit()
 * @ingroup forms
 */
function artesian_post_form($form, &$form_state, $post, $thread = NULL) {

  // Save the post entity to the form state so we can access it from other
  // form processing functions. If it already exists, grab it.
  if (isset($form_state['post'])) {
    $post = $form_state['post'];
  }
  else {
    $form_state['post'] = $post;
  }

  // Assign the form class for theming.
  $form['#attributes']['class'][] = 'post-form';
  if (!empty($post->type)) {
    $form['#attributes']['class'][] = 'post-' . $post->type . '-form';
  }

  // Basic post information.
  // These elements are just values so they are not even sent to the client.
  foreach (array('thread_id', 'post_id') as $key) {
    $form[$key] = array(
      '#type' => 'value',
      '#value' => isset($post->$key) ? $post->$key : NULL,
    );
  }

  // Add the field API fields for this post entity subtype.
  field_attach_form('artesian_post', $post, $form, $form_state);

  // Title of the post.
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => (empty($post->title)) ? '' : $post->title,
    '#weight' => -20,
    '#required' => FALSE,
  );

  $form['author'] = array(
    '#type' => 'fieldset',
    '#title' => 'Author options',
    '#weight' => 20,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  // Get an easier to type reference to the author object.
  $author = $post->author();

  // The user name of the actual poster (usually -- can be overridden).
  $form['author']['author_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Posted by'),
    '#maxlength' => 60,
    '#autocomplete_path' => 'user/autocomplete',
    '#default_value' => $author->authorName,
    '#weight' => 10,
    '#description' => t('Leave blank for %anonymous.', array('%anonymous' => variable_get('anonymous', t('Anonymous')))),
  );

  $form['author']['post_as_anonymous'] = array(
    '#type' => 'checkbox',
    '#title' => t('Post anonymously?'),
    '#description' => t('If checked, post will be made anonymously using the display name below.'),
    '#default_value' => $author->authorDisplayID === 0 ? 1 : 0,
    '#required' => FALSE,
    '#weight' => 20,
  );

  $form['author']['author_display_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Displayed author name'),
    '#description' => t('This is the name that will show up as the author.'),
    '#default_value' => !empty($author->authorDisplayName) ? $author->authorDisplayName : $author->authorName,
    '#maxlength' => 60,
    '#size' => 30,
    '#required' => FALSE,
    '#weight' => 30,
  );


// POST OPTIONS ---------------------------------------------------------------\

  $form['post_options'] = array(
    '#type' => 'fieldset',
    '#title' => 'Post options',
    '#weight' => 30,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['post_options']['date'] = array(
    '#type' => 'textfield',
    '#title' => t('Posted on'),
    '#maxlength' => 30,
    '#default_value' => !empty($post->date) ? $post->date : $post->created,
    '#weight' => 30,
  );

  $form['post_options']['hide_signature'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide your signature on this post. (registered users only)'),
    '#default_value' => isset($post->hide_signature) ? $post->hide_signature : 0,
    '#weight' => 10,
  );

  $form['post_options']['published'] = array(
    '#type' => 'checkbox',
    '#title' => t('Published'),
    '#description' => t('Note that unpublishing the first post in a thread will unpublish the entire thread.'),
    '#default_value' => isset($post->published) ? $post->published : 1,
    '#weight' => 20,
  );

// THREAD OPTIONS -------------------------------------------------------------\

  $form['thread_options'] = array(
    '#type' => 'fieldset',
    '#title' => 'Thread options',
    '#weight' => 40,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['thread_options']['sticky'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sticky'),
    '#default_value' => isset($post->sticky) ? $post->sticky : 0,
    '#weight' => 10,
  );

  $form['thread_options']['announcement'] = array(
    '#type' => 'checkbox',
    '#title' => t('Announcement'),
    '#default_value' => isset($post->announcement) ? $post->announcement : 0,
    '#weight' => 20,
  );

  $form['thread_options']['closed'] = array(
    '#type' => 'checkbox',
    '#title' => t('Closed'),
    '#default_value' => isset($post->closed) ? $post->closed : 0,
    '#weight' => 30,
  );

// BUTTONS --------------------------------------------------------------------\

  $form['buttons'] = array();
  $form['buttons']['#weight'] = 100;
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Post reply'),
    '#weight' => 10,
    '#submit' => array('artesian_post_form_submit'),
  );

  if (!$post->isNew()) {
    $form['buttons']['delete'] = array(
      '#access' => user_access('delete posts'),
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#weight' => 20,
      '#submit' => array('artesian_post_form_delete_submit'),
    );
  }

// THREAD REVIEW --------------------------------------------------------------\

  if (!empty($thread) && $thread_review = $thread->renderReview()) {
    $form['thread_review'] = array(
      '#type' => 'fieldset',
      '#title' => 'Thread review',
      '#weight' => 40,
      '#tree' => TRUE,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['thread_review']['threads'] = array(
      '#markup' => $thread_review,
      '#weight' => 200,
    );
  }

  return $form;
}

/**
 * Form validation handler for artesian_post_add_form().
 *
 * Uses Field API to handle validation of attached fields.
 *
 * @see artesian_post_add_form_submit()
 */
function artesian_post_form_validate($form, &$form_state) {
  $post = $form_state['post'];

  field_attach_form_validate('post', $post, $form, $form_state);
}

/**
 * Prepares the post object for submission.
 *
 * Currently, this just handles the authorship but is named to allow expansion.
 *
 * @param $form
 * @param array $form_state
 */
function artesian_post_form_prepare_submit($form, &$form_state) {
  // Assign some form state values to an easier reference variable.
  $author = $form_state['post']->author();
  $author_name = $form_state['values']['author_name'];
  $author_display_name = $form_state['values']['author_display_name'];

  // Assign authorship of the post.
  if (empty($author_name)) {
    // Post is being submitted by (or on behalf of) an anoymous user.

    // Set both the author ID and display author ID to 0 and pass in the name.
    $author->setAuthor(0, 0, $author_display_name);
  }
  else {
    // Post is being submitted by (or on behalf of) an authenticated user.

    // Load the user account associated with the given author name.
    $account = user_load_by_name($author_name);

    // As a failsafe, ensure the name corresponds to a valid user account.
    if (empty($account)) {
      throw new Exception("Invalid user name");
    }

    if ($form_state['values']['post_as_anonymous']) {
      // Authenticated user but posting anonymously. Set the actual user
      // for the author but set the display ID to 0 (anonymous) and pass in the
      //chosen display name.
      $author->setAuthor($account, 0, $author_display_name);
    }
    else {
      // Authenticated user, not anonymous. Use real values for everything.
      $author->setAuthor($account);
    }
  }
}

/**
 * Form submission handler for artesian_post_form().
 *
 * @see artesian_post_form_validate()
 */
function artesian_post_form_submit($form, &$form_state) {
  // Artesian's submit code is in its own function to allow it to be called
  // from the thread object submit as well in the case of the anchor post.
  artesian_post_form_prepare_submit($form, $form_state);

  $post = $form_state['post'];

  // This core function copies all the form values into the entity object and
  // also handles submitting the field API field data. Note that this will fail
  // to reach values in fieldsets if the fieldset has #tree = TRUE set.
  entity_form_submit_build_entity('artesian_post', $post, $form, $form_state);

  // Save the post entity.
  $result = $post->save();

  $form_state['redirect'] = $post->url();

}

/**
 * Button submit function: handle the 'Delete' button on the post form.
 */
function artesian_post_form_delete_submit(&$form, &$form_state) {
  $destination = array();
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }

  $post = $form_state['post'];

  $form_state['redirect'] = $post->deleteURL();
}

/**
 * Menu callback -- ask for confirmation of post deletion
 */
function artesian_post_delete_confirm($form, &$form_state, $post) {
  // If this is the anchor post, we want to go to the thread delete instead.
  // This should only happen if someone typed in the delete URL manually.
  if ($post->anchor) {
    return artesian_thread_delete_confirm($form, $form_state, $post);
  }

  $form_state['post'] = $post;

  return confirm_form($form,
    t('Are you sure you want to delete this post?'),
    $post->viewURL(),
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Execute post deletion
 */
function artesian_post_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $post = $form_state['post'];

    $title = $post->label();
    $title = (empty($title)) ? t("Post #@post_id", array("@post_id" => $post->id())) : $title;
    $type = $post->entityType();
    $redirect = $post->thread()->viewURL();

    entity_delete('artesian_post', $post->id());

    watchdog('artesian', '@type: deleted %title.', array('@type' => $type, '%title' => $title));
    drupal_set_message(t('@type %title has been deleted.', array('@type' => $type, '%title' => $title)));
  }

  $form_state['redirect'] = $redirect;
}
