<?php
/**
 * @file
 * Default theme implementation to display a post.
 *
 * Available variables:
*/
?>

<div id="post-<?php print $post_id; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
 <div class="forum-post-info clearfix">
    <div class="forum-posted-on">
      <?php print $posted_on ?>
    </div>

    <?php if (!empty($in_reply_to)): ?>
   	 <span class="forum-in-reply-to"><?php print $in_reply_to; ?></span>
    <?php endif; ?>

    <?php // Add a note when a post is unpublished so it doesn't rely on theming. ?>
    <?php if (!$post->published): ?>
      <span class="unpublished-post-note"><?php print t("Unpublished post") ?></span>
    <?php endif; ?>

    <span class="forum-post-number"><?php print $permalink; ?></span>
  </div> <?php // End of post info div ?>

  <div class="forum-post-wrapper">
    <div class="forum-post-panel-sub">
      <?php if (!empty($author_pane)): ?>
        <?php print $author_pane; ?>
      <?php endif; ?>
    </div>

    <div class="forum-post-panel-main clearfix">
      <?php if (!empty($title)): ?>
        <div class="forum-post-title">
          <?php print $title ?>
        </div>
      <?php endif; ?>

      <div class="forum-post-content">
        <?php
          print render($content);
        ?>
      </div>

      <?php if (!empty($post_edited)): ?>
        <div class="post-edited">
          <?php print $post_edited ?>
        </div>
      <?php endif; ?>

      <?php if (!empty($signature)): ?>
        <div class="author-signature">
          <?php print $signature ?>
        </div>
      <?php endif; ?>
    </div>
  </div> <?php // End of post wrapper div ?>

  <div class="forum-post-footer clearfix">
    <div class="forum-jump-links">
      <a href="#forum-topic-top" title="<?php print t('Jump to top of page'); ?>" class="af-button-small"><span><?php print t("Top"); ?></span></a>
    </div>

    <div class="forum-post-links">
      <?php print $edit_link ?>
      <?php print $delete_link ?>
      <?php print $reply_link ?>
    </div>
  </div> <?php // End of footer div ?></div>